from django.db import models
from multiselectfield import MultiSelectField

# Create your models here.
GEEKS_CHOICES = (
    ("1", "Sunday"),
    ("2", "Monday"),
    ("3", "Tuesday"),
    ("4", "Wednesday"),
    ("5", "Thuresday"),
    ("6", "Friday"),
    ("7", "Saturday"),

)

class Hospital(models.Model):
    name = models.CharField(max_length=250,)
    address = models.CharField(max_length=250)
    phone = models.TextField(max_length=10)
    email = models.EmailField(max_length=250)
    pan_vat = models.CharField(max_length=12)

    def __str__(self):
        return self.name

class Department(models.Model):
    department_name = models.CharField(max_length=255)

    def __str__(self):
        return self.department_name


class Specialazation(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Doctor(models.Model):
    name = models.CharField(max_length=250,)
    address = models.CharField(max_length=250)
    phone = models.TextField(max_length=12)
    email = models.EmailField(max_length=250)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    hospital = models.ForeignKey(Hospital, on_delete=models.CASCADE)
    specialazation = models.ForeignKey(Specialazation, on_delete=models.CASCADE)
    time = models.DateField(auto_now=False, auto_now_add=False)
    my_field = MultiSelectField(choices=GEEKS_CHOICES)

    def __str__(self):
        return self.name