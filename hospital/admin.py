from django.contrib import admin

# Register your models here.
from .models import Hospital,Doctor,Specialazation,Department

admin.site.register(Hospital)
admin.site.register(Doctor)
admin.site.register(Specialazation)
admin.site.register(Department)

